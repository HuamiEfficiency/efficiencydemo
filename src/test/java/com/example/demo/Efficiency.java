package com.example.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;
import java.util.*;


public class Efficiency {

	public static void main(String args[]){
		Efficiency test = new Efficiency();
        static int a = 10;
        static int b = 11;
        System.out.println(test.sum(a,b));
        System.out.println(test.minus(a,b));
	}

	public int sum(int a, int b){
        return a+b;
    }

    public int minus(int a, int b){
        return a-b;
    }
}
